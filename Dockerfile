FROM arm32v7/busybox:uclibc
MAINTAINER Justin Wood <justin.b.wood@gmail.com>

RUN wget https://github.com/prometheus/prometheus/releases/download/v2.2.0/prometheus-2.2.0.linux-armv7.tar.gz -O /tmp/prometheus.tar.gz && \
	mkdir -p /tmp/prometheus && \
	tar xf /tmp/prometheus.tar.gz -C /tmp/prometheus --strip 1 && \
	mkdir -p /etc/prometheus && \
	cp /tmp/prometheus/prometheus /bin/prometheus && \
	cp /tmp/prometheus/promtool /bin/promtool && \
	cp /tmp/prometheus/prometheus.yml /etc/prometheus/prometheus.yml && \
	mkdir -p /usr/share/prometheus && \
	cp -R /tmp/prometheus/console_libraries/ /usr/share/prometheus/ && \
	cp -R /tmp/prometheus/consoles/ /usr/share/prometheus/ && \
	rm /tmp/prometheus.tar.gz && \
	rm -rf /tmp/prometheus

EXPOSE		9090
VOLUME		[ "/prometheus" ]
WORKDIR 	/prometheus
ENTRYPOINT	[ "/bin/prometheus" ]
CMD		[ "--config.file=/etc/prometheus/prometheus.yml", \
             	  "--storage.tsdb.path=/prometheus", \
             	  "--web.console.libraries=/usr/share/prometheus/console_libraries", \
             	  "--web.console.templates=/usr/share/prometheus/consoles" ]
